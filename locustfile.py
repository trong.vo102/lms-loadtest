import json
from locust import HttpUser, User, task

tenant = "abc"

# login info
username = 'lmslms1'
password = 'lmslms'

class SimulateUser(HttpUser):
    def __init__(self, parent):
        super(SimulateUser, self).__init__(parent)
        self.token = ""
        self.headers = {"Content-Type": "application/json", "X-TENANT-ID": tenant, "Accept": "application/json"}

    def on_start(self):
        response = self.client.post(
            "/login/", data=json.dumps({'username': username, 'password': password}), headers=self.headers)
        self.token = response.json()["token"]
        self.headers["authorization"] = "Bearer " + self.token

    @task
    def getCourses(self):
        self.client.get("/courses/?page=1&size=10", headers=self.headers)

    @task
    def getCourseDetails(self):
        self.client.get("/courses/1", headers=self.headers)

    @task
    def getClassesInCourse(self):
        self.client.get("/courses/1/classes/?page=1&size=10", headers=self.headers)

    @task
    def getLearningContentInCourse(self):
        self.client.get("/courses/1/learning_content", headers=self.headers)

    @task
    def getUnitsInCourse(self):
        self.client.get("/courses/1/learning_content/units", headers=self.headers)

    # todo: đề thi

    @task
    def getUnitsInCourse(self):
        self.client.get("/courses/1/resources/textbooks?keyword=", headers=self.headers)

    @task
    def getClasses(self):
        self.client.get("/classes/?page=1&size=10", headers=self.headers)
    
    @task
    def getClassDetails(self):
        self.client.get("/classes/1", headers=self.headers)

    @task
    def getSchedulerInClass(self):
        self.client.get("/classes/1/scheduler/session/?dateTime=2022-09-03T15:45:27.717Z&previous=0&after=2", headers=self.headers)

    @task
    def getAnnouncementsInClass(self):
        self.client.get("/classes/1/announcement/received", headers=self.headers)

    @task
    def getStudentsInClass(self):
        self.client.get("/classes/1/students?page=1&size=10", headers=self.headers)

    @task
    def getTeachersInClass(self):
        self.client.get("/classes/1/teachers", headers=self.headers)
    
    @task
    def getLearningContentInClass(self):
        self.client.get("/classes/1/learning_content", headers=self.headers)

    @task
    def getAttendanceInClass(self):
        self.client.get("/classes/1/overall_attendance", headers=self.headers)

    @task
    def getQuizzesInClass(self):
        self.client.get("/classes/1/learning_content/quizzes", headers=self.headers)

    @task
    def getPostsInClass(self):
        self.client.get("/classes/1/posts", headers=self.headers)

    @task
    def getPostDetailsInClass(self):
        self.client.get("/classes/1/posts/2", headers=self.headers)

    @task
    def getPostDetailsInClass(self):
        self.client.get("/grades/result?scope=CLASS&scopeId=1", headers=self.headers)

    @task
    def getStudents(self):
        self.client.get("/students", headers=self.headers)

    @task
    def getStudentDetails(self):
        self.client.get("/students/15", headers=self.headers)

    @task
    def getStaffs(self):
        self.client.get("/staffs", headers=self.headers)

    @task
    def getStaffDetails(self):
        self.client.get("/staffs/4", headers=self.headers)