import json
import random
import string
from locust import HttpUser, User, task, between

tenant = 'abc'

# login info
password = 'lmslms'

class SimulateUser(HttpUser):
    def __init__(self, parent):
        super(SimulateUser, self).__init__(parent)
        self.token = ''
        self.headers = {'Content-Type': 'application/json', 'X-TENANT-ID': tenant, 'Accept': 'application/json'}
        self.questions = {}
        self.session = ''

        self.username = ''

    def on_start(self):
        self.username = 'trong'+str(random.randint(1,100))+'@gmail.com'
        response = self.client.post(
            '/login/', data=json.dumps({'username': self.username, 'password': password}), headers=self.headers)
        self.token = response.json()['token']
        self.headers['authorization'] = 'Bearer ' + self.token

        self.startSession()

    def on_stop(self):
        self.submitSession()

    
    def submitSession(self):
        response = self.client.get('/classes/1/learning_content/quizzes/1', headers=self.headers)
        if response.status_code == 401:
            self.stop()
        body = response.json()
        if (not body['isAllowedToInitSession']):
            with self.client.post('/class/1/quiz/1/session/init', headers=self.headers, catch_response=True) as response:
                if response.status_code == 400:
                    self.stop()
                body = response.json()

                self.questions = response.json()['listData']
                self.session = body['id']
                self.client.post('/class/1/quiz/1/session/'+self.session+'/end', headers=self.headers)
        

    def startSession(self):
        response = self.client.get('/classes/1/learning_content/quizzes/1', headers=self.headers)
        if response.status_code == 401:
            self.stop()
        body = response.json()
        if (body['isAllowedToInitSession']):
            with self.client.post('/class/1/quiz/1/session/init', headers=self.headers, catch_response=True) as response:
                if response.status_code == 400:
                    self.stop()
                body = response.json()

                self.questions = response.json()['listData']
                self.session = body['id']
        else:
            self.session = body['unEndedSessionId']
            with self.client.get('/class/1/quiz/1/session/'+self.session+'?page=1&size=10', headers=self.headers, catch_response=True) as response:
                if response.status_code == 401 or response.status_code == 400:
                    response.success()
                    self.stop()
                self.questions = response.json()['listData']

    def answerMultiChoice(self, question):
        self.client.post('/course/1/class/1/quiz/1/session/'+self.session+'/question/'+str(question['id']), data=json.dumps({"id":question['id'],"description":question['description'],"point":question['point'],"answerList": question['answerList'],"isMultipleAnswer":question['isMultipleAnswer'],"answers":[random.randrange(0,len(question['answerList']))+1],"optionalStudentNote":""}), headers=self.headers)

    def answerFillInBlank(self, question):
        answer = question
        answer['optionalStudentNote'] = ""
        answer['answers'] = []
        for blank in question['blankList']:
            answerForBlank = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))
            answer['answers'].append(answerForBlank)
        self.client.post('/course/1/class/1/quiz/1/session/'+self.session+'/question/'+str(question['id']), data=json.dumps(answer), headers=self.headers)
    
    def answerFillInBlankWithChoice(self, question):
        answer = question
        answer['optionalStudentNote'] = ""
        answer['answers'] = []
        for blank in question['blankList']:
            answerList = blank['answerList']
            answer['answers'].append(random.randrange(0,len(answerList)))
        self.client.post('/course/1/class/1/quiz/1/session/'+self.session+'/question/'+str(question['id']), data=json.dumps(answer), headers=self.headers)

    def answerFillInBlankDragAndDrop(self, question):
        answer = question
        answer['optionalStudentNote'] = ""
        answer['answers'] = []
        for blank in question['blankList']:
            answerList = question['answerList']
            answer['answers'].append(random.randrange(0,len(answerList))+1)
        self.client.post('/course/1/class/1/quiz/1/session/'+self.session+'/question/'+str(question['id']), data=json.dumps(answer), headers=self.headers)

    def answerWriting(self, question):
        answer = question
        answer['optionalStudentNote'] = ""
        answer['answers'] = [''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))]
        self.client.post('/course/1/class/1/quiz/1/session/'+self.session+'/question/'+str(question['id']), data=json.dumps(answer), headers=self.headers)


    @task
    def answerQuestion(self):
        question = self.questions[random.randrange(0, len(self.questions))]
        if (question['type'] == 'MULTI_CHOICE'):
            self.answerMultiChoice(question)
        elif (question['type'] == 'FILL_IN_BLANK'):
            self.answerFillInBlank(question)
        elif (question['type'] == 'FILL_IN_BLANK_WITH_CHOICES'):
            self.answerFillInBlankWithChoice(question)
        elif (question['type'] == 'FILL_IN_BLANK_DRAG_AND_DROP'):
            self.answerFillInBlankDragAndDrop(question)
        elif (question['type'] == 'WRITING'):
            self.answerWriting(question)