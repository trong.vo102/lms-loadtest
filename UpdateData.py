import json
import random
from locust import HttpUser, User, task, between

tenant = 'abc'

# login info
username = 'lmslms1'
password = 'lmslms'

class SimulateUser(HttpUser):
    wait_time = between(1, 5)
    def __init__(self, parent):
        super(SimulateUser, self).__init__(parent)
        self.token = ''
        self.headers = {'Content-Type': 'application/json', 'X-TENANT-ID': tenant, 'Accept': 'application/json'}

    def on_start(self):
        response = self.client.post(
            '/login/', data=json.dumps({'username': username, 'password': password}), headers=self.headers)
        self.token = response.json()['token']
        self.headers['authorization'] = 'Bearer ' + self.token

    def getRandomClass(self):
        response = self.client.get('/classes/?page=1&size=10', headers=self.headers)
        body = response.json()
        classes = body['listData']
        if (len(classes) == 0): 
            return None
        return classes[random.randrange(0, len(classes))]
    
    def getRandomCourse(self):
        response = self.client.get('/courses/?page=1&size=10', headers=self.headers)
        body = response.json()
        courses = body['listData']
        if (len(courses) == 0): 
            return None
        return courses[random.randrange(0, len(courses))]

    @task
    def getCourses(self):
        self.client.get('/courses/?page=1&size=10', headers=self.headers)

    @task
    def getCourseDetails(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        self.client.get('/courses/'+str(course['id']), headers=self.headers)

    @task
    def getClassesInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        self.client.get('/courses/'+str(course['id'])+'/classes/?page=1&size=10', headers=self.headers)

    @task
    def getLearningContentInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        self.client.get('/courses/'+str(course['id'])+'/learning_content', headers=self.headers)

    @task
    def getUnitsInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        self.client.get('/courses/'+str(course['id'])+'/learning_content/units', headers=self.headers)
    # todo: đề thi

    @task
    def getUnitsInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        self.client.get('/courses/'+str(course['id'])+'/resources/textbooks?keyword=', headers=self.headers)

    @task
    def getClasses(self):
        self.client.get('/classes/?page=1&size=10', headers=self.headers)
    
    @task
    def getClassDetails(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id']), headers=self.headers)

    @task
    def getSchedulerInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/scheduler/session/?dateTime=2022-09-03T15:45:27.717Z&previous=0&after=2', headers=self.headers)

    @task
    def getAnnouncementsInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/announcement/received', headers=self.headers)

    @task
    def getStudentsInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/students?page=1&size=10', headers=self.headers)

    @task
    def getTeachersInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/teachers', headers=self.headers)
    
    @task
    def getLearningContentInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/learning_content', headers=self.headers)

    @task
    def getAttendanceInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/overall_attendance', headers=self.headers)

    @task
    def getQuizzesInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/learning_content/quizzes', headers=self.headers)

    @task
    def getPostsInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/classes/'+str(clazz['id'])+'/posts', headers=self.headers)

    @task
    def getPostDetailsInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/posts', headers=self.headers)
        body = response.json()
        posts = body['listData']
        if (len(posts) == 0): 
            return
        post = posts[random.randrange(0, len(posts))]
        self.client.get('/classes/'+str(clazz['id'])+'/posts/'+str(post['id']), headers=self.headers)

    @task
    def getPostDetailsInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.get('/grades/result?scope=CLASS&scopeId='+str(clazz['id']), headers=self.headers)

    @task
    def getStudents(self):
        self.client.get('/students', headers=self.headers)

    @task
    def getStudentDetails(self):
        response = self.client.get('/students', headers=self.headers)
        body = response.json()
        students = body['data']['listData']
        if (len(students) == 0): 
            return None
        student = students[random.randrange(0, len(students))]
        self.client.get('/students/'+str(student['id']), headers=self.headers)

    @task
    def getStaffs(self):
        self.client.get('/staffs', headers=self.headers)

    @task
    def getStaffDetails(self):
        response = self.client.get('/staffs', headers=self.headers)
        body = response.json()
        staffs = body['listData']
        if (len(staffs) == 0): 
            return None
        staff = staffs[random.randrange(0, len(staffs))]
        self.client.get('/staffs/'+str(staff['id']), headers=self.headers)

    ################ Thêm data

    @task
    def announcement(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.post('/classes/'+str(clazz['id'])+'/announcement', data=json.dumps({'content':'<p>Ngày mai là lễ chúng ta nghỉ nhé, lịch học bù sẽ được thông báo sau.</p>','id':0,'senderId':1,'senderName':'','sentAt':'2022-09-08T20:06:51.950Z','subject':'Thông báo nghỉ lễ','tags':'Nghỉ lễ','receiversId':[],'sendMailAsCopy':False,'seen':False,'readOnly':False}), headers=self.headers)

    @task
    def post(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        self.client.post('/classes/'+str(clazz['id'])+'/posts/', data=json.dumps({"title":"Bài lúc sáng khó quá ạ","content":"<p>Câu 7 làm như nào vậy mọi nguời</p>"}), headers=self.headers)

    @task
    def deletePost(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/posts', headers=self.headers)
        body = response.json()
        posts = body['listData']
        if (len(posts) == 0): 
            return
        post = posts[random.randrange(0, len(posts))]
        self.client.delete("/classes/"+str(clazz['id'])+"/posts/"+str(post['id']), headers=self.headers)

    @task
    def comment(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/posts', headers=self.headers)
        body = response.json()
        posts = body['listData']
        if (len(posts) == 0): 
            return
        post = posts[random.randrange(0, len(posts))]
        self.client.post("/classes/"+str(clazz['id'])+"/posts/"+str(post['id'])+'/comments', data=json.dumps({"content":"<p>Áp dụng công thức là ra</p>"}), headers=self.headers)

    # @task
    # def likePost(self):
    #     self.client.post('/classes/2/posts/3/interaction', data=json.dumps({"type":"UP_VOTE"}), headers=self.headers)

    # @task
    # def dislikePost(self):
    #     self.client.post('/classes/2/posts/3/interaction', data=json.dumps({"type":"DOWN_VOTE"}), headers=self.headers)

    @task
    def addNewUnitInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        self.client.post('/classes/'+str(clazz['id'])+'/learning_content/chapters/'+str(chapter['id'])+'/units', data=json.dumps({"type":"unit","title":"Bài 5","textbooks":[],"content":"<p>Bài này quan trọng</p>","id":-1,"order":-1,"attachment":"[{\"type\":\"Video\",\"url\":\"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/system%2Fmovie.mp4?alt=media&token=681f7ddf-f911-4cc8-bd92-7529ac3f2b28\",\"name\":\"movie.mp4\"}]"}), headers=self.headers)

    @task
    def addNewQuizInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        self.client.post('/classes/'+str(clazz['id'])+'/learning_content/chapters/'+str(chapter['id'])+'/quizzes', data=json.dumps({"id":-1,"title":"Bài kiểm tra 1 tiết","description":"Bài này khó lắm","tag":"Kiểm tra 1 tiết","type":"quiz","order":-1,"examId":1}), headers=self.headers)

    @task
    def deleteUnitInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        units = chapter['units']
        units = list(filter(lambda q: not q['isFromCourse'], units))
        if (len(units) == 0): 
            return
        unit = units[random.randrange(0, len(units))]
        self.client.delete('/classes/'+str(clazz['id'])+'/learning_content/units/'+str(unit['id']), headers=self.headers)

    @task
    def deleteQuizInClass(self):
        clazz = self.getRandomClass()
        if (clazz == None):
            return
        response = self.client.get('/classes/'+str(clazz['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        quizzes = chapter['quizzes']
        quizzes = list(filter(lambda q: not q['isFromCourse'], quizzes))
        if (len(quizzes) == 0): 
            return
        quiz = quizzes[random.randrange(0, len(quizzes))]
        self.client.delete('/classes/'+str(clazz['id'])+'/learning_content/quizzes/'+str(quiz['id']), headers=self.headers)
        

    @task 
    def addSyllabus(self):
        self.client.post('/resources/textbook', data=json.dumps({"name":"Giáo trình loadtest","author":"ngoctrong","attachment":"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/system%2FLVTN_HK213_38.docx?alt=media&token=82bf879f-0239-4495-b8d8-de80d150b50a","description":"Sách hay"}), headers=self.headers)

    @task 
    def deleteSyllabus(self):
        response = self.client.get('/resources/textbook?page=1&size=100', headers=self.headers)
        body = response.json()
        textbooks = body['listData']
        if (len(textbooks) == 0): 
            return
        textbook = textbooks[random.randrange(0, len(textbooks))]
        self.client.delete('/resources/textbook/'+str(textbook['id']), headers=self.headers)

    @task 
    def addSyllabusForCourse(self):
        response = self.client.get('/resources/textbook?page=1&size=10', headers=self.headers)
        body = response.json()
        textbooks = body['listData']
        if (len(textbooks) == 0): 
            return
        textbook = textbooks[random.randrange(0, len(textbooks))]
        course = self.getRandomCourse()
        if (course == None):
            return
        self.client.post('/courses/'+str(course['id'])+'/resources/textbooks', data=json.dumps({"idList":[textbook['id']]}), headers=self.headers)
        
    @task(5)
    def deleteSyllabusForCourse(self):
        course = self.getRandomCourse()
        if (course == None):
            return
        response = self.client.get('/courses/'+str(course['id'])+'/resources/textbooks?keyword=', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        textbookid = body[random.randrange(0, len(body))]['id']
        self.client.delete('/courses/'+str(course['id'])+'/resources/textbooks/'+str(textbookid), headers=self.headers)
        
    @task
    def addNewUnitInCourse(self):
        course = self.getRandomCourse()
        if (course == None):
            return
        response = self.client.get('/courses/'+str(course['id'])+'/learning_content', headers=self.headers)
        chapters = response.json()
        if (len(chapters) == 0):
            return
        chapter = chapters[random.randrange(0, len(chapters))]
        self.client.post('/courses/'+str(course['id'])+'/learning_content/chapters/'+str(chapter['id'])+'/units', data=json.dumps({"type":"unit","title":"Bài học về môi trường","textbooks":[],"content":"<p>abc: 123</p>","id":-1,"order":-1,"attachment":"[{\"type\":\"Video\",\"url\":\"https://firebasestorage.googleapis.com/v0/b/bklms-47f9b.appspot.com/o/system%2Fmovie.mp4?alt=media&token=546b0276-f9f3-4cea-a32a-74f38bf70668\",\"name\":\"movie.mp4\"}]"}), headers=self.headers)

    @task
    def addNewQuizInCourse(self):        
        course = self.getRandomCourse()
        if (course == None):
            return
        response = self.client.get('/courses/'+str(course['id'])+'/learning_content', headers=self.headers)
        chapters = response.json()
        if (len(chapters) == 0):
            return
        chapter = chapters[random.randrange(0, len(chapters))]

        response = self.client.get('/course/'+str(course['id'])+'/exam/search?page=1&size=10', headers=self.headers)
        exams = response.json()['listData']
        if (len(exams) == 0):
            return
        exam = exams[random.randrange(0, len(exams))]
        self.client.post('/courses/'+str(course['id'])+'/learning_content/chapters/'+str(chapter['id'])+'/quizzes', data=json.dumps({"id":-1,"title":"Kiểm tra 15'","description":"Bài này khó lắm","tag":"Kiểm tra 15'","type":"quiz","order":-1,"examId":exam['id']}), headers=self.headers)

    @task
    def deleteUnitInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        response = self.client.get('/courses/'+str(course['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        units = chapter['units']
        if (len(units) == 0): 
            return
        unit = units[random.randrange(0, len(units))]
        self.client.delete('/courses/'+str(course['id'])+'/learning_content/units/'+str(unit['id']), headers=self.headers)

    @task
    def deleteQuizInCourse(self):
        course = self.getRandomCourse();
        if (course == None):
            return
        response =self.client.get('/courses/'+str(course['id'])+'/learning_content', headers=self.headers)
        body = response.json()
        if (len(body) == 0): 
            return
        chapter = body[random.randrange(0, len(body))]
        quizzes = chapter['quizzes']
        if (len(quizzes) == 0): 
            return
        quiz = quizzes[random.randrange(0, len(quizzes))]
        self.client.delete('/courses/'+str(course['id'])+'/learning_content/quizzes/'+str(quiz['id']), headers=self.headers)